package com.aryo.mongo.tekpromongodb.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.aryo.mongo.tekpromongodb.model.Mahasiswa;
import com.aryo.mongo.tekpromongodb.repository.MahasiswaRepository;

@Service
public class MahasiswaService {

    private final MahasiswaRepository mahasiswaRepository;

    public MahasiswaService(MahasiswaRepository mahasiswaRepository) {
        this.mahasiswaRepository = mahasiswaRepository;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.insert(mahasiswa);
    }

    public void updateMahasiswa(Mahasiswa mahasiswa) {
        Mahasiswa savedMahasiswa = mahasiswaRepository.findById(mahasiswa.getId())
                .orElseThrow(() -> new RuntimeException(String.format("ID tidak ditemukan %d", mahasiswa.getId())));
        savedMahasiswa.setNama(mahasiswa.getNama());
        savedMahasiswa.setNIM(mahasiswa.getNIM());
        savedMahasiswa.setSemester(mahasiswa.getSemester());
        savedMahasiswa.setJenjang(mahasiswa.getJenjang());
        savedMahasiswa.setJurusan(mahasiswa.getJurusan());

        mahasiswaRepository.save(savedMahasiswa);
    }

    public List<Mahasiswa> getAllMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    public Mahasiswa getMahasiswaByNIM(String nim) {
        return mahasiswaRepository.findBynim(nim)
                .orElseThrow(() -> new RuntimeException(String.format("nim %s tidak ditemukan", nim)));
    }

    public void deleteMahasiswa(String id) {
        mahasiswaRepository.deleteById(id);
    }

    public void deleteAllMahasiswa() {
        mahasiswaRepository.deleteAll();
    }
}
