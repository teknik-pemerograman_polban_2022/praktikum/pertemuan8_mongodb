package com.aryo.mongo.tekpromongodb.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aryo.mongo.tekpromongodb.model.Mahasiswa;
import com.aryo.mongo.tekpromongodb.service.MahasiswaService;

@RestController
@RequestMapping("/api/Mahasiswa")
public class MahasiswaController {
    private final MahasiswaService mahasiswaService;

    public MahasiswaController(MahasiswaService mahasiswaService) {
        this.mahasiswaService = mahasiswaService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity addMahasiswa(@RequestBody Mahasiswa mahasiswa) {
        mahasiswaService.addMahasiswa(mahasiswa);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping
    public ResponseEntity updateMahasiswa(@RequestBody Mahasiswa mahasiswa) {
        mahasiswaService.updateMahasiswa(mahasiswa);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<Mahasiswa>> getAllMahasiswa() {
        return ResponseEntity.ok(mahasiswaService.getAllMahasiswa());
    }

    @GetMapping(value = "/{nim}")
    public ResponseEntity<Mahasiswa> getMahasiswaByNIM(@PathVariable String nim) {
        return ResponseEntity.ok(mahasiswaService.getMahasiswaByNIM(nim));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteMahasiswa(@PathVariable String id) {
        mahasiswaService.deleteMahasiswa(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/all")
    public ResponseEntity deleteAllMahasiswa() {
        mahasiswaService.deleteAllMahasiswa();
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
