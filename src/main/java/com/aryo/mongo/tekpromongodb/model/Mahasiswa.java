package com.aryo.mongo.tekpromongodb.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("Mahasiswa")
public class Mahasiswa {
    @Id
    @Indexed(unique = true)
    private String id;
    @Field(name = "name")
    private String Nama;
    @Indexed(name = "nim", unique = true)
    private String NIM;
    @Field(name = "semester")
    private int Semester;
    @Field(name = "jurusan")
    private String Jurusan;
    @Field(name = "jenjang")
    private String Jenjang;

    public String getJurusan() {
        return this.Jurusan;
    }

    public void setJurusan(String Jurusan) {
        this.Jurusan = Jurusan;
    }

    public String getJenjang() {
        return this.Jenjang;
    }

    public void setJenjang(String Jenjang) {
        this.Jenjang = Jenjang;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return this.Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public String getNIM() {
        return this.NIM;
    }

    public void setNIM(String NIM) {
        this.NIM = NIM;
    }

    public int getSemester() {
        return this.Semester;
    }

    public void setSemester(int semester) {
        this.Semester = semester;
    }

}
