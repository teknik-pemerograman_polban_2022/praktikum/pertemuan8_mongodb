package com.aryo.mongo.tekpromongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TekproMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(TekproMongodbApplication.class, args);
	}

}
