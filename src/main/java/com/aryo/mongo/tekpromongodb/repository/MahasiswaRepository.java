package com.aryo.mongo.tekpromongodb.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.aryo.mongo.tekpromongodb.model.Mahasiswa;

public interface MahasiswaRepository extends MongoRepository<Mahasiswa, String> {
    @Query("{'NIM': ?0}")
    Optional<Mahasiswa> findBynim(String NIM);
}
